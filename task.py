import pandas as pd
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt

# Загрузим данные (покупки клиентов: сумма покупок, количество покупок)
data = {
    'Сумма': [200, 150, 400, 600, 250, 300, 700, 1000, 800, 900],
    'Количество_покупок': [2, 1, 3, 4, 2, 2, 5, 6, 4, 5]
}

df = pd.DataFrame(data)

# Масштабируем данные
scaler = StandardScaler()
scaled_data = scaler.fit_transform(df)

# Применяем метод k-средних для кластеризации на 3 кластера
kmeans = KMeans(n_clusters=3, random_state=42)
kmeans.fit(scaled_data)

# Добавляем метки кластеров в исходный датафрейм
df['Кластер'] = kmeans.labels_

# Выведем результаты кластеризации
print(df)

# Визуализируем кластеры
plt.scatter(df['Сумма'], df['Количество_покупок'], c=df['Кластер'], cmap='rainbow')
plt.xlabel('Сумма покупок')
plt.ylabel('Количество покупок')
plt.title('Кластеры клиентов')
plt.show()
